#导包
import time
import unittest
from Scripts.test01_login import TestLogin
from Tools.HTMLTestRunner import HTMLTestRunner

#组装测试套件
suite = unittest.TestSuite()
suite.addTest(unittest.makeSuite(TestLogin))
#指定测试报告路径
# report = "./report/report-{}.html".format(time.strftime("%Y%m%d-%H%M%S"))
report = "./report/report.html"
#打开文件流
with open(report,"wb") as f:
    runner = HTMLTestRunner(f,title="API_Report")
#创建HTMLTestRunner运行器
    runner.run(suite)
