#添加员工

#修改员工

#查询员工

#删除员工


#导包

import requests
import app
#创建接口类
class EmployeeAPI:
    #初始化
    def __init__(self):
        self.url_employee = app.BASE_URL+"/api/sys/user"  #登录
        self.url_update_employee = app.BASE_URL+"/api/sys/user/{}"#修改
        self.url_get_employee = app.BASE_URL+"/api/sys/user/{}"#查询
        self.url_delete_employee = app.BASE_URL+"/api/sys/user/{}"#删除

    # 添加员工
    def add_employee(self,add_employee_data):
        print(app.headers_data)
        return requests.post(self.url_employee,json=add_employee_data,headers=app.headers_data)
    # 修改员工
    def update_employee(self,employee_id,update_data):
        url = self.url_update_employee.format(employee_id)
        return requests.put(url=url,json=update_data,headers = app.headers_data)
    # 查询员工
    def get_employee(self,employee_id):
        url = self.url_get_employee,format(employee_id)
        return requests.get(url=url,headers=app.herders_data)
    # 删除员工
    def delete_employee(self,employee_id):
        url = self.url_get_employee,format(employee_id)
        return requests.delete(url,headers=app.herders_data)