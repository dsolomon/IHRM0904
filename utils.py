
#封装公共断言
def common_assert(case,response,status_code,success,code,message):
    case.assertEqual(status_code, response.status_code)
    case.assertEqual(success, response.json().get("success"))
    case.assertEqual(code, response.json().get("code"))
    case.assertIn(message, response.json().get("message"))

def login_for_employee():
    def setUp(self) :
        self.login_api = LoginAPI()
    #后置处理
    def tearDown(self) :
        pass