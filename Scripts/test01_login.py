#导包
import unittest

import app
from API.login import LoginAPI

#创建测试类
class TestLogin(unittest.TestCase):
    #前置处理
    def setUp(self) :
        self.login_api = LoginAPI()
    #后置处理
    def tearDown(self) :
        pass
    #定义测试用例
    def test01_case001(self):
        #调用登录接口
        response= self.login_api.login({"mobile":"13800000002","password":"123456"})
        print(response.json())
        self.assertEqual(200,response.status_code)
        self.assertEqual(True,response.json().get("success"))
        self.assertEqual(10000,response.json().get("code"))
        self.assertIn("操作成功",response.json().get("message"))

        #提取token信息
        app.TOKEN ="Bearer " + response.json().get("data")
        print(app.TOKEN)
        #追加headers_
        app.headers_data["Authorization"] = app.TOKEN
#定义测试用例
    def test01_case002(self):
        #调用登录接口
        response= self.login_api.login({"mobile":"","password":"123456"})
        print(response.json())
        self.assertEqual(200,response.status_code)
        self.assertEqual(False,response.json().get("success"))
        self.assertEqual(20001,response.json().get("code"))
        self.assertIn("用户名或密码错误",response.json().get("message"))
#定义测试用例
    def test01_case003(self):
        #调用登录接口
        response= self.login_api.login({"mobile":"13800000002","password":""})
        print(response.json())
        self.assertEqual(200,response.status_code)
        self.assertEqual(False,response.json().get("success"))
        self.assertEqual(20001,response.json().get("code"))
        self.assertIn("用户名或密码错误",response.json().get("message"))
#定义测试用例
    def test01_case004(self):
        #调用登录接口
        response= self.login_api.login({"mobile":"1380000000","password":"123456"})
        print(response.json())
        self.assertEqual(200,response.status_code)
        self.assertEqual(False,response.json().get("success"))
        self.assertEqual(20001,response.json().get("code"))
        self.assertIn("用户名或密码错误",response.json().get("message"))
#定义测试用例
    def test01_case005(self):
        #调用登录接口
        response= self.login_api.login({"mobile":"138000000022","password":"123456"})
        print(response.json())
        self.assertEqual(200,response.status_code)
        self.assertEqual(False,response.json().get("success"))
        self.assertEqual(20001,response.json().get("code"))
        self.assertIn("用户名或密码错误",response.json().get("message"))
#定义测试用例
    def test01_case006(self):
        #调用登录接口
        response= self.login_api.login({"mobile":"error","password":"123456"})
        print(response.json())
        self.assertEqual(200,response.status_code)
        self.assertEqual(False,response.json().get("success"))
        self.assertEqual(20001,response.json().get("code"))
        self.assertIn("用户名或密码错误",response.json().get("message"))
#定义测试用例  输入未注册的账号
    def test01_case007(self):
        #调用登录接口
        response= self.login_api.login({"mobile":"13800000112","password":"123456"})
        print(response.json())
        self.assertEqual(200,response.status_code)
        self.assertEqual(False,response.json().get("success"))
        self.assertEqual(20001,response.json().get("code"))
        self.assertIn("用户名或密码错误",response.json().get("message"))
#定义测试用例
    def test01_case008(self):
        #调用登录接口
        response= self.login_api.login({"mobile":"13800000002","password":"123456","parameter":"hahahaha"})
        print(response.json())
        self.assertEqual(200,response.status_code)
        self.assertEqual(True,response.json().get("success"))
        self.assertEqual(10000,response.json().get("code"))
        self.assertIn("操作成功",response.json().get("message"))
        # 定义测试用例

    def test01_case009(self):
        # 调用登录接口
        response = self.login_api.login({ "password": "123456"})
        print(response.json())
        self.assertEqual(200, response.status_code)
        self.assertEqual(False, response.json().get("success"))
        self.assertEqual(20001, response.json().get("code"))
        self.assertIn("用户名或密码错误", response.json().get("message"))
#定义测试用例
    def test01_case010(self):
        #调用登录接口
        response= self.login_api.login({"mobile":"13800000002"})
        print(response.json())
        self.assertEqual(200,response.status_code)
        self.assertEqual(False,response.json().get("success"))
        self.assertEqual(20001,response.json().get("code"))
        self.assertIn("用户名或密码错误",response.json().get("message"))
#定义测试用例
    def test01_case011(self):
        #调用登录接口
        response= self.login_api.login(None)
        print(response.json())
        self.assertEqual(200,response.status_code)
        self.assertEqual(False,response.json().get("success"))
        self.assertEqual(99999,response.json().get("code"))
        self.assertIn("抱歉，系统繁忙，请稍后重试！",response.json().get("message"))
#定义测试用例
    def test01_case012(self):
        #调用登录接口
        response= self.login_api.login({"mobil":"13800000002","password":"123456"})
        print(response.json())
        self.assertEqual(200,response.status_code)
        self.assertEqual(False,response.json().get("success"))
        self.assertEqual(20001,response.json().get("code"))
        self.assertIn("用户名或密码错误",response.json().get("message"))
#定义测试用例
    def test01_case013(self):
        #调用登录接口
        response= self.login_api.login({"mobil":"13800000002","passwor":"123456"})
        print(response.json())
        self.assertEqual(200,response.status_code)
        self.assertEqual(False,response.json().get("success"))
        self.assertEqual(20001,response.json().get("code"))
        self.assertIn("用户名或密码错误",response.json().get("message"))
