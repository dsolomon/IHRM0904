# #导包
#
# import unittest
# from API.employ import EmployeeAPI
# from API.login import LoginAPI
# import json
# from utils import common_assert
# import app
#
# class TestEmployee(unittest.TestCase):
#
#     employee_id = None
#
#     def setUp(self):
#         self.employee_api = EmployeeAPI()
#         self.login_api = LoginAPI()
#     def tearDown(self):
#         pass
#
#     #添加员工测试用例
#     def test01_add_employ(self):
#         add_employee_data = {
#         "username":"jack0906008",
#         "mobile":"13211998076",
#         "timeOfEntry":"2020-07-09",
#         "formOfEmployment":1,
#         "workNumber":"100101110111",
#         "departmentName":"销售",
#         "departmentId":"1266699057968001024",
#         "correctionTime":"2020-07-30T16:00:00.000Z"
#         }
#         #获取响应结果
#         response = self.employee_api.add_employee(add_employee_data=add_employee_data)
#         print(response.json())
#         #断言响应状态码
#         #公共断言方法
#         common_assert(self,response,200,True,10000,"操作成功")
#         #提取员工ID
#         TestEmployee.employee_id = response.json().get("data").get("id")
#     #修改员工测试用例
#     def test02_update_employ(self):
#         update_employee_data = {"username":"rose0906"}
#         #获取响应结果
#         response = self.employee_api.update_employee(9002,update_data=update_employee_data)
#         print(response.json())
#         #断言响应状态码
#         self.assertEqual(200,response.status_code)
#         self.assertEqual(True,response.json().get("success"))
#         self.assertEqual(10000,response.json().get("code"))
#         self.assertIn("操作成功",response.json().get("message"))
#
#         # 修改员工测试用例
#     def test03_get_employ(self,employee_id):
#         url = self.employee_api.get_employee(TestEmployee.employee_id)
#         # 获取响应结果
#         response = self.employee_api.get_employee(employee_id=TestEmployee.employee_id)
#         print(response.json())
#         # 断言响应状态码
#         self.assertEqual(200, response.status_code)
#         self.assertEqual(True, response.json().get("success"))
#         self.assertEqual(10000, response.json().get("code"))
#         self.assertIn("操作成功", response.json().get("message"))
#
#         # 删除员工测试用例
#     def test04_delete_employ(self, employee_id):
#             url = self.employee_api.get_employee(TestEmployee.employee_id)
#             # 获取响应结果
#             response = self.employee_api.get_employee(employee_id=TestEmployee.employee_id)
#             print(response.json())
#             # 断言响应状态码
#             self.assertEqual(200, response.status_code)
#             self.assertEqual(True, response.json().get("success"))
#             self.assertEqual(10000, response.json().get("code"))
#             self.assertIn("操作成功", response.json().get("message"))