#导包
import unittest
import requests
import json
from API.login import LoginAPI
from parameterized import parameterized
#构架测试数据
def build_data():
    #login_data,status_code,success,code,message
    test_data = []
    #指定文件路径
    json_file = "../data/login.json"
    #打开json文件
    with open(json_file,encoding="utf-8") as f:
        json_data = json.load(f)
        for case_data in json_data:
            login_data = case_data.get("login_data")
            print(login_data)
            status_code = case_data.get("status_code")
            success = case_data.get("success")
            code = case_data.get("code")
            message = case_data.get("message")
            test_data.append((login_data, status_code, success, code, message))
            print("test_data={}".format(login_data, status_code, success, code, message))
    return test_data
#创建测试类
class TestLogin(unittest.TestCase):
    #前置处理
    def setUp(self) :
        self.login_api = LoginAPI()
    #后置处理
    def tearDown(self) :
        pass
    #定义测试用例
    @parameterized.expand(build_data)
    def test01_case001(self,login_data,status_code,success,code,message):
        #调用登录接口
        response= self.login_api.login(login_data)
        print(response.json())
        self.assertEqual(status_code,response.status_code)
        self.assertEqual(success,response.json().get("success"))
        self.assertEqual(code,response.json().get("code"))
        self.assertIn(message,response.json().get("message"))
